use super::trie_node::TrieNode;

#[derive(Clone, Debug, Eq, PartialEq, Default)]
pub struct Trie<T>
    where
        T: Ord + Clone,
{
    root: Option<Box<TrieNode<T>>>,
}

impl<T> Trie<T>
    where
        T: Ord + Clone,
{
    pub fn new() -> Self {
        Self { root: Option::None }
    }

    // fn insert_word(&mut self, values: &[&T]) {
    //     if values.len() == 0 {
    //         self.root.as_ref().unwrap().find_sibling(values[0]);
    //     }
    // }
}


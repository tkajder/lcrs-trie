use std::cmp::Ordering;
use std::mem;
use std::slice::Iter;

#[derive(Debug, Eq, PartialEq, Clone)]
pub struct TrieNode<T: Ord + Clone> {
    value: T,
    child: Option<Box<TrieNode<T>>>,
    sibling: Option<Box<TrieNode<T>>>,
}

impl<T: Ord + Clone> TrieNode<T> {
    pub fn new(value: &T) -> Self {
        TrieNode {
            value: value.clone(),
            child: Option::None,
            sibling: Option::None,
        }
    }

    pub fn find_sibling(&self, value: &T) -> Option<&Self> {
        self.find_sibling_by(value, T::eq)
    }

    pub fn find_sibling_by<F: Fn(&T, &T) -> bool>(&self, value: &T, f: F) -> Option<&Self> {
        let mut current = Some(self);
        while let Some(node) = current {
            if f(&node.value, value) {
                return Some(node);
            }

            // Navigate to next node by flattening the box to the ref - if it is there
            current = node.sibling.as_ref().map(|b| b.as_ref());
        }

        return None;
    }

    pub fn find_sibling_mut(&mut self, value: &T) -> Option<&mut Self> {
        self.find_sibling_mut_by(value, T::eq)
    }

    pub fn find_sibling_mut_by<F: Fn(&T, &T) -> bool>(&mut self, value: &T, f: F) -> Option<&mut Self> {
        let mut sibling = Some(self);
        while let Some(node) = sibling {
            if f(&node.value, value) {
                return Some(node);
            }

            sibling = node.sibling.as_mut().map(|b| b.as_mut());
        }

        return None;
    }

    pub fn find_sibling_or_adjacent(&self, value: &T) -> (&Self, Ordering) {
        self.find_sibling_or_adjacent_by(value, T::cmp)
    }

    pub fn find_sibling_or_adjacent_by<F: Fn(&T, &T) -> Ordering>(&self, value: &T, compare: F) -> (&Self, Ordering) {
        let mut current = self;
        while current.sibling.is_some() {
            match compare(&current.value, value) {
                Ordering::Less => {
                    // Safe unwrap due to guarantee of Some by while condition
                    current = current.sibling.as_ref().unwrap()
                }
                Ordering::Equal => return (current, Ordering::Equal),
                Ordering::Greater => return (current, Ordering::Greater),
            }
        }

        return (current, compare(&current.value, value));
    }

    pub fn find_sibling_or_adjacent_mut(&mut self, value: &T) -> (&mut Self, Ordering) {
        self.find_sibling_or_adjacent_mut_by(value, T::cmp)
    }

    pub fn find_sibling_or_adjacent_mut_by<F: Fn(&T, &T) -> Ordering>(&mut self, value: &T, compare: F) -> (&mut Self, Ordering) {
        let mut current = self;
        while current.sibling.is_some() {
            match compare(&current.value, value) {
                Ordering::Less => {
                    // Safe unwrap due to guarantee to be Some in while condition
                    current = current.sibling.as_mut().unwrap()
                }
                Ordering::Equal => return (current, Ordering::Equal),
                Ordering::Greater => return (current, Ordering::Greater),
            }
        }

        // Compute ordering early to avoid double borrow
        let current_ordering = compare(&current.value, value);
        return (current, current_ordering);
    }

    // TODO: Want to think about this function some more, specifically does a `find_or_create_sibling_by` make sense?
    fn find_or_create_sibling(&mut self, value: &T) -> &Self {
        return match self.find_sibling_or_adjacent_mut(value) {
            (sibling, Ordering::Less) => {
                // Replace "my sibling" with a new node, and make the new node have a sibling of "my sibling"
                let prev_sibling = sibling.sibling.replace(Box::new(TrieNode::new(value)));
                sibling.sibling.as_mut().unwrap().sibling = prev_sibling;
                sibling
            }
            (sibling, Ordering::Equal) => sibling,
            (sibling, Ordering::Greater) => {
                // Replace "me" (my memory) with a new node with "me" as the sibling
                let prev_head = mem::replace(sibling, TrieNode::new(value));
                sibling.sibling = Some(Box::new(prev_head));
                sibling
            }
        };
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    macro_rules! test_data {
        () => {
            TrieNode {
                value: 2,
                child: None,
                sibling: Some(Box::new(TrieNode {
                    value: 4,
                    child: None,
                    sibling: Some(Box::new(TrieNode {
                        value: 6,
                        child: None,
                        sibling: Some(Box::new(TrieNode {
                            value: 8,
                            child: None,
                            sibling: None,
                        })),
                    })),
                })),
            };
        };
    }

    macro_rules! find_sibling_tests {
        ($($module_id:ident: ($is_some:expr, $value:expr),)+) => {
            mod find_sibling_tests {
                use super::*;

                $(
                    mod $module_id {
                        use super::*;

                        #[test]
                        fn test_find_sibling() {
                            let test_data = test_data!();

                            match (test_data.find_sibling(&$value), $is_some) {
                                (Some(found_sibling), true) => assert_eq!($value, found_sibling.value, "expected found sibling to have value {}, but was {}", $value, found_sibling.value),
                                (Some(found_sibling), false) => panic!("expected to not find sibling with value {}, but found {:?}", $value, found_sibling),
                                (None, true) => panic!("expected to find sibling with value {}, but found None", $value),
                                (None, false) => {}
                            }
                        }

                        #[test]
                        fn test_find_sibling_mut() {
                            let mut test_data = test_data!();

                            match (test_data.find_sibling_mut(&$value), $is_some) {
                                (Some(found_sibling), true) => assert_eq!($value, found_sibling.value, "expected found sibling to have value {}, but was {}", $value, found_sibling.value),
                                (Some(found_sibling), false) => panic!("expected to not find sibling with value {}, but found {:?}", $value, found_sibling),
                                (None, true) => panic!("expected to find sibling with value {}, but found None", $value),
                                (None, false) => {}
                            }
                        }
                    }
                )+
            }
        };
    }

    find_sibling_tests!(
        present_start_node: (true, 2),
        present_middle_node: (true, 4),
        present_end_node: (true, 8),
        non_present_value_low: (false, 0),
        non_present_value_middle: (false, 5),
        non_present_value_high: (false, 10),
    );

    macro_rules! find_sibling_or_adjacent_tests {
        ($($module_id:ident: ($expected_value:expr, $expected_ordering:expr, $value:expr),)+) => {
            mod find_sibling_or_adjacent_tests {
                use super::*;

                $(
                    mod $module_id {
                        use super::*;

                        #[test]
                        fn test_find_sibling_or_adjacent() {
                            let test_data = test_data!();

                            match test_data.find_sibling_or_adjacent(&$value) {
                                (found_sibling, Ordering::Less) => {
                                    assert_eq!(Ordering::Less, $expected_ordering, "expected ordering to be {:?}, but was {:?}", $expected_ordering, Ordering::Less);
                                    assert_eq!($expected_value, found_sibling.value, "expected found sibling to have value {}, but was {}", $expected_value, found_sibling.value);
                                },
                                (found_sibling, Ordering::Equal) => {
                                    assert_eq!(Ordering::Equal, $expected_ordering, "expected ordering to be {:?}, but was {:?}", $expected_ordering, Ordering::Equal);
                                    assert_eq!($expected_value, found_sibling.value, "expected found sibling to have value {}, but was {}", $expected_value, found_sibling.value);
                                },
                                (found_sibling, Ordering::Greater) => {
                                    assert_eq!(Ordering::Greater, $expected_ordering, "expected ordering to be {:?}, but was {:?}", $expected_ordering, Ordering::Greater);
                                    assert_eq!($expected_value, found_sibling.value, "expected found sibling to have value {}, but was {}", $expected_value, found_sibling.value);
                                },
                            }
                        }

                        #[test]
                        fn test_find_sibling_or_adjacent_mut() {
                            let mut test_data = test_data!();

                            match test_data.find_sibling_or_adjacent_mut(&$value) {
                                (found_sibling, Ordering::Less) => {
                                    assert_eq!(Ordering::Less, $expected_ordering, "expected ordering to be {:?}, but was {:?}", $expected_ordering, Ordering::Less);
                                    assert_eq!($expected_value, found_sibling.value, "expected found sibling to have value {}, but was {}", $expected_value, found_sibling.value);
                                },
                                (found_sibling, Ordering::Equal) => {
                                    assert_eq!(Ordering::Equal, $expected_ordering, "expected ordering to be {:?}, but was {:?}", $expected_ordering, Ordering::Equal);
                                    assert_eq!($expected_value, found_sibling.value, "expected found sibling to have value {}, but was {}", $expected_value, found_sibling.value);
                                },
                                (found_sibling, Ordering::Greater) => {
                                    assert_eq!(Ordering::Greater, $expected_ordering, "expected ordering to be {:?}, but was {:?}", $expected_ordering, Ordering::Greater);
                                    assert_eq!($expected_value, found_sibling.value, "expected found sibling to have value {}, but was {}", $expected_value, found_sibling.value);
                                },
                            }
                        }
                    }
                )+
            }
        };
    }

    find_sibling_or_adjacent_tests!(
        less_than_start_node: (2, Ordering::Greater, 0),
        equal_to_start_node: (2, Ordering::Equal, 2),
        equal_to_mid_node: (6, Ordering::Equal, 6),
        equal_to_end_node: (8, Ordering::Equal, 8),
        // The expected behavior is to return the "greater" value rather than the "lesser" for example
        // (4, Ordering::Greater) rather than (2, Ordering::Less) for input 3 though both nodes are adjacent.
        less_than_mid_node: (4, Ordering::Greater, 3),
        greater_than_end_node: (8, Ordering::Less, 10),
    );

    #[test]
    fn test_new() {
        let expected_value = TrieNode {
            value: 'x',
            child: Option::None,
            sibling: Option::None,
        };

        assert_eq!(expected_value, TrieNode::new(&'x'));
    }

    #[test]
    fn test() {
        let mut trie_node = TrieNode {
            value: 1,
            child: Option::None,
            sibling: Option::Some(Box::new(TrieNode::new(&32))),
        };

        eprintln!("{:?}", trie_node);
        eprintln!("{:?}", trie_node.find_or_create_sibling(&2));
        eprintln!("{:?}", trie_node);
    }
}

# lcrs-trie

A left-child, right-sibling trie data structure library.

## Motivations

This project is a work in progress towards two personal goals:

- Understanding ownership in Rust
- A building block data structure for a solver for the game Boggle 

## Usage

When this data structure nears version 1.0.0 the README.md will be updated to reflect the general usability of the library.

## License

Licensed under either of

 * Apache License, Version 2.0
   ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license
   ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.

## Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted for inclusion in the work by you, as defined in the Apache-2.0 license, shall be dual licensed as above, without any additional terms or conditions.

